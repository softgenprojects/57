import { Box, VStack, Heading, Text, useColorModeValue, Center, SimpleGrid, Button, Link } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <Center py="12" px={{ base: '4', lg: '8' }}>
        <VStack spacing="8">
          <BrandLogo />
          <Heading as="h1" size="xl" textAlign="center">
            Ihr Notar und Rechtsanwalt in Frankfurt
          </Heading>
          <Text textAlign="center" maxW="560px">
            Herzlich willkommen auf der Webseite von Notar Dr. Gronstedt LL.M.. Wir freuen uns, dass Sie uns gefunden haben. Auf den folgenden Seiten wollen wir Ihnen unser Team und unsere Vorstellungen davon, was wir für Sie im Bereich der notariellen Dienstleistungen tun können, vorstellen.
          </Text>
        </VStack>
      </Center>
      <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10} px={{ base: '4', lg: '8' }} py="12">
        <Box>
          <Heading size="md" mb="4">Über Uns</Heading>
          <Text>
            Notar Dr. Gronstedt ist eine Kanzlei, welche mitten im Herzen Frankfurts in unmittelbarer Nähe zum Opernplatz und der Alten Oper gelegen ist. Seit 2008 konzentriert sich die Kanzlei Notar Dr. Gronstedt ganz auf die Betreuung ihrer Mandanten in notariellen Angelegenheiten.
          </Text>
        </Box>
        <Box>
          <Heading size="md" mb="4">Unsere Mission</Heading>
          <Text>
            Ziel und Anspruch unserer Einheit ist es, ein Notariat zu bilden, welches höchste Qualität mit einer individuellen Betreuung unserer Mandanten verbindet. Dabei bringen wir unsere internationale Erfahrung ein.
          </Text>
        </Box>
      </SimpleGrid>
      <Center py="12" px={{ base: '4', lg: '8' }}>
        <VStack spacing="8">
          <Heading as="h2" size="lg" textAlign="center">
            Unsere Kernkompetenzen
          </Heading>
          <Text textAlign="center" maxW="560px">
            Finden Sie auf den nächsten Seiten heraus, worin wir unsere Kernkompetenzen sehen und wie wir Sie bei Ihrem Anliegen am besten betreuen können.
          </Text>
          <Button colorScheme="pink" size="lg" as={Link} href="#">
            Mehr erfahren
          </Button>
        </VStack>
      </Center>
    </Box>
  );
};

export default Home;
