const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createInvestor = async (walletAddress, email) => {
  try {
    const investor = await prisma.investor.create({
      data: {
        walletAddress,
        email
      }
    });
    return investor;
  } catch (error) {
    if (error.code === 'P2002') {
      throw new Error('Investor with the provided wallet address or email already exists.');
    }
    throw error;
  }
};

module.exports = { createInvestor };
