const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createInvestor = async (walletAddress, email) => {
  try {
    const investor = await prisma.investor.create({
      data: {
        walletAddress,
        email,
      },
    });
    return investor;
  } catch (error) {
    if (error.code === 'P2002') {
      throw new Error('Investor with provided wallet address or email already exists');
    }
    throw error;
  }
};

const getInvestorWallet = async (investorId) => {
  return await prisma.wallet.findMany({
    where: {
      investorId,
    },
  });
};

const updateWalletBalance = async (walletId, newBalance) => {
  return await prisma.wallet.update({
    where: {
      id: walletId,
    },
    data: {
      balance: newBalance,
    },
  });
};

module.exports = { createInvestor, getInvestorWallet, updateWalletBalance };