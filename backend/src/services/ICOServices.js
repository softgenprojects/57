const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createICO = async ({ name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth }) => {
  return await prisma.iCO.create({
    data: {
      name,
      description,
      startTime,
      endTime,
      tokenName,
      tokenSymbol,
      totalTokens,
      tokensPerEth
    }
  });
};

const getActiveICOs = async () => {
  const now = new Date();
  return await prisma.iCO.findMany({
    where: {
      startTime: {
        lte: now
      },
      endTime: {
        gte: now
      }
    }
  });
};

const investInICO = async ({ investorId, icoId, amount }) => {
  return await prisma.investor.update({
    where: { id: investorId },
    data: {
      Wallet: {
        create: {
          balance: amount,
          icoId: icoId
        }
      }
    }
  });
};

const finalizeICO = async (icoId) => {
  return await prisma.iCO.update({
    where: { id: icoId },
    data: { status: 'Finalized' }
  });
};

module.exports = { createICO, getActiveICOs, investInICO, finalizeICO };