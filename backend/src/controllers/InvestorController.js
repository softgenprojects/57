const { createInvestor } = require('../services/InvestorServices.js');

const registerInvestor = async (req, res) => {
  const { walletAddress, email } = req.body;
  try {
    const investor = await createInvestor(walletAddress, email);
    res.status(201).json(investor);
  } catch (error) {
    res.status(500).json({ message: 'Error registering investor', error });
  }
};

const getWalletDetails = async (req, res) => {
  const { investorId } = req.params;
  try {
    const wallets = await getInvestorWallet(investorId);
    res.status(200).json(wallets);
  } catch (error) {
    res.status(500).json({ message: 'Error fetching wallet details', error });
  }
};

const updateWallet = async (req, res) => {
  const { walletId, newBalance } = req.body;
  try {
    const updatedWallet = await updateWalletBalance(walletId, newBalance);
    res.status(200).json(updatedWallet);
  } catch (error) {
    res.status(500).json({ message: 'Error updating wallet balance', error });
  }
};

module.exports = { registerInvestor, getWalletDetails, updateWallet };