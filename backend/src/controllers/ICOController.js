const ICOServices = require('@services/ICOServices');

const createICO = async (req, res) => {
  try {
    const { name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth } = req.body;
    const newICO = await ICOServices.createICO({ name, description, startTime, endTime, tokenName, tokenSymbol, totalTokens, tokensPerEth });
    res.status(201).json(newICO);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getActiveICOs = async (req, res) => {
  try {
    const activeICOs = await ICOServices.getActiveICOs();
    res.status(200).json(activeICOs);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const investInICO = async (req, res) => {
  try {
    const { investorId, icoId, amount } = req.body;
    const investmentResult = await ICOServices.investInICO({ investorId, icoId, amount });
    res.status(200).json(investmentResult);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const finalizeICO = async (req, res) => {
  try {
    const { icoId } = req.body;
    const finalizationResult = await ICOServices.finalizeICO(icoId);
    res.status(200).json(finalizationResult);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = { createICO, getActiveICOs, investInICO, finalizeICO };