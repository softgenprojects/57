const express = require('express');
const router = express.Router();
const { createInvestor } = require('@services/InvestorServices.js');

const registerInvestor = async (req, res) => {
  const { walletAddress, email } = req.body;
  if (!walletAddress || !email) {
    return res.status(400).json({
      message: 'Missing required fields: walletAddress and email'
    });
  }
  try {
    const newInvestor = await createInvestor(walletAddress, email);
    res.status(201).json({
      message: 'Investor registered successfully',
      data: newInvestor
    });
  } catch (error) {
    res.status(500).json({
      message: 'Error registering investor',
      error: error.message
    });
  }
};

module.exports = { registerInvestor };