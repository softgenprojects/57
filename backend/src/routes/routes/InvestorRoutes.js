const express = require('express');
const router = express.Router();
const { registerInvestor } = require('@controllers/InvestorController');

router.post('/register', registerInvestor);

module.exports = router;