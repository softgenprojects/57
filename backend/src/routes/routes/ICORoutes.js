const express = require('express');
const router = express.Router();
const ICOController = require('@controllers/ICOController');
const { authMiddleware } = require('@middleware/authMiddleware');
const { validateRequest } = require('../middleware/validationMiddleware');

router.post('/topic/create', authMiddleware, validateRequest, ICOController.createICO);
router.get('/topic/active', authMiddleware, ICOController.getActiveICOs);
router.post('/topic/invest', authMiddleware, validateRequest, ICOController.investInICO);
router.post('/topic/finalize', authMiddleware, validateRequest, ICOController.finalizeICO);

module.exports = router;