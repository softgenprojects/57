const express = require('express');
const router = express.Router();
const { createICO, getActiveICOs, investInICO, finalizeICO } = require('../controllers/ICOController');
const authMiddleware = require('../middleware/authMiddleware');
const { validateRequest } = require('../middleware/validationMiddleware');

router.post('/ico/create', authMiddleware, validateRequest, createICO);
router.get('/ico/active', authMiddleware, getActiveICOs);
router.post('/ico/invest', authMiddleware, validateRequest, investInICO);
router.post('/ico/finalize', authMiddleware, validateRequest, finalizeICO);

module.exports = router;