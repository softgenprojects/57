const express = require('express');
const { registerInvestor } = require('../controllers/InvestorController');

const router = express.Router();

router.post('/investor/register', registerInvestor);

module.exports = router;