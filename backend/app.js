require('module-alias/register');
const express = require('express');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors({
  origin: ['http://localhost:3080', process.env.CORS_FRONTEND_URL || 'http://localhost:3000', 'http://65.108.219.251:3080/'],
  optionsSuccessStatus: 200,
  credentials: true 
}));

const ICORoutes = require('./src/routes/ICORoutes');
const InvestorRoutes = require('./src/routes/InvestorRoutes');

app.use('/api', ICORoutes);
app.use('/api', InvestorRoutes);

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

const port = process.env.BE_PORT || 8080;
let server;

function startServer() {
  if (server) {
    server.close(() => {
      console.log('Server restarted.');
      server = app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
      });
    });
  } else {
    server = app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  }
}

startServer();